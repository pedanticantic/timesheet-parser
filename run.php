<?php

use TimesheetParser\TimesheetValidator;

require_once __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

// @TODO: Allow a path to be passed in to override the path in the .env file.
if (isset($_ENV['FILE_PATH'])) {
    $timesheet = TimesheetValidator::parse($_ENV['FILE_PATH']);
} else {
    throw new RuntimeException('Please configure the file path in the .env file (FILE_PATH)');
}

if ($timesheet->isValid()) {
    $output = $timesheet->toArray();
} else {
    $output = $timesheet->errorsToArray();
}

foreach ($output as $outputLine) {
    echo $outputLine.PHP_EOL;
}
