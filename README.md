# timesheet-parser

This parses and validates my timesheet files.

## What it is not

This is not what you're looking for. It is not a time recording system.

## What it is

This repo is just for parsing my timesheet files, checking they are valid and then doing some basic calculations on it.

My timesheets look like this:

```text
31/03/2022 [1.00]

08:45-09:00 = 0.25
    Catching up
09:00-09:30 = 0.50
    Planning my next task
09:30-09:45 = 0.25
    PRO-1234 - do some work on the PRO project

01/04/2022 []

09:00-09:15 = 0.25
    Start of a new month
```

The main class in this repo loads the file contents and determines the type of each line (start/end/length or work description). It then checks that the length is consistent with the start/end times, and then checks that no entry starts earlier than an earlier entry. It also calculates the total time for the timesheet.

It keeps track of any errors, and there is a method that tells you if there are any errors.

There are effectively a "toString()" (well, more correctly, "toArray()") methods that will convert the timesheet contents to text. The "errors" one show the content with the errors embeded in it; the "normal" one shows the content with any gaps between entries (eg because of lunch breaks) and the total time.
