<?php

namespace Tests\Lines;

use TimesheetParser\Lines\DescriptionLine;
use DigiTicketsTests\AbstractTestCase;

class DescriptionLineTest extends AbstractTestCase
{
    public function testCreation()
    {
        // Every DescriptionLine is deemed to be valid.
        $content = 'Some description';
        $asArray = [$content];
        $descriptionLine = new DescriptionLine($content);

        $this->assertTrue($descriptionLine->isValid());
        $this->assertEquals($descriptionLine->toArrayWithErrors(), $asArray);
        $this->assertEquals($descriptionLine->toArray(), $asArray);
    }
}
