<?php

namespace Tests\Lines;

use TimesheetParser\Lines\DateLine;
use DigiTicketsTests\AbstractTestCase;

class DateLineTest extends AbstractTestCase
{
    public function creationProvider(): array
    {
        return [
            'valid date, no length' => ['01/06/2021'],
            'invalid date, no length' => ['01/99/2021', null, true],
            'valid date, with length' => ['01/06/2021', 3.75],
            'invalid date, with length' => ['01/99/2021', 4.25, true],
            'valid date, with length greater than 10 hours' => ['01/06/2021', 10.25, true], // @TODO: I haven't run it with this data set.
        ];
    }

    /**
     * @param string $testDate
     * @param float|null $dayLength
     * @param bool $expectError
     * @dataProvider creationProvider
     */
    public function testCreation(string $testDate, float $dayLength = null, bool $expectError = false)
    {
        $content = $testDate.' ['.$dayLength.']';
        $dateLine = new DateLine($content);

        if ($expectError) {
            $this->assertFalse($dateLine->isValid());
        } else {
            $this->assertTrue($dateLine->isValid());
            if (!is_null($dayLength)) {
                // If we populated the day length, validate it and check the line is still valid.
                $dateLine->checkDailyTime($dayLength);
                $this->assertTrue($dateLine->isValid());
            }
            $this->assertEquals($testDate, $dateLine->getTimesheetDate()->format('d/m/Y'));
        }
    }
}
