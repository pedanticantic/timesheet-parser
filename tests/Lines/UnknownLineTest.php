<?php

namespace Tests\Lines;

use TimesheetParser\Lines\UnknownLine;
use DigiTicketsTests\AbstractTestCase;

class UnknownLineTest extends AbstractTestCase
{
    public function testCreation()
    {
        // Every UnknownLine is deemed to be invalid.
        $content = 'Any content';
        $unknownLine = new UnknownLine($content);

        $this->assertFalse($unknownLine->isValid());
        $this->assertEquals(
            $unknownLine->toArrayWithErrors(),
            [
                $content,
                '^-- Line is of unknown type',
                ''
            ]
        );
    }
}
