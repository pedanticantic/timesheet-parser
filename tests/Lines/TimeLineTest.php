<?php

namespace Tests\Lines;

use DateTimeImmutable;
use TimesheetParser\Lines\TimeLine;
use DigiTicketsTests\AbstractTestCase;

class TimeLineTest extends AbstractTestCase
{
    public function creationProvider(): array
    {
        $today = new DateTimeImmutable();
        $todayYMD = $today->format('Y-m-d');
        $tomorrow = $today->add(new \DateInterval('P1D'));
        $tomorrowYMD = $tomorrow->format('Y-m-d');
        $startDate1 = new DateTimeImmutable($todayYMD.' 10:00:00.000000');
        $startDate2 = new DateTimeImmutable($todayYMD.' 00:00:00.000000');
        $endDate1 = new DateTimeImmutable($todayYMD.' 08:45:00.000000');
        $endDate2 = new DateTimeImmutable($todayYMD.' 10:00:00.000000');
        $endDate3 = new DateTimeImmutable($todayYMD.' 11:15:00.000000');
        $endDate4 = new DateTimeImmutable($tomorrowYMD.' 00:00:00.000000');

        return [
            'End before start' => [$startDate1, $endDate1, 1.25, true, 1.25],
            'End same as start' => [$startDate1, $endDate2, 0.00, false, 0.00],
            'End after start' => [$startDate1, $endDate3, 1.25, false, 1.25],
            'End after start, wrong length' => [$startDate1, $endDate3, 2.75, false, 1.25],
            'End at midnight' => [$startDate1, $endDate4, 14.00, false, 14.00],
            'End at midnight, invalid length' => [$startDate1, $endDate4, 9.00, false, 14.00],
            'Start at midnight' => [$startDate2, $endDate1, 8.75, false, 8.75],
        ];
    }

    /**
     * @param DateTimeImmutable $startDateTime
     * @param DateTimeImmutable $endDateTime
     * @param float $suppliedLength
     * @param bool $expectInvalidTimes
     * @param float $correctLength
     * @dataProvider creationProvider
     */
    public function testCreation(
        DateTimeImmutable $startDateTime,
        DateTimeImmutable $endDateTime,
        float $suppliedLength,
        bool $expectInvalidTimes,
        float $correctLength
    ) {
        // Instantiate the line. The content has a very specific format.
        $content = sprintf('%s-%s = %0.2f', $startDateTime->format('H:i'), $endDateTime->format('H:i'), $suppliedLength);
        $timeLine = new TimeLine($content);

        // Check that it calculated the start and end times correctly.
        // Note that we check the full date & time in each case.
        $this->assertEquals($startDateTime, $timeLine->getStartTime());
        $this->assertEquals($endDateTime, $timeLine->getEndTime());

        // Check it interpreted the supplied length correctly.
        $this->assertEquals($suppliedLength, $timeLine->getLength());

        // Check the cross-field validation.
        $expectValid = true;
        $expectedArrayWithErrors = [];
        $expectedArrayWithErrors[] = $content;
        if ($expectInvalidTimes) {
            $expectValid = false;
            $expectedArrayWithErrors[] = '^-- Start time must not be later than end time';
        }
        if ($correctLength != $suppliedLength) {
            $expectValid = false;
            $expectedArrayWithErrors[] = sprintf('^-- Length is not correct. Should be %0.2f', $correctLength);
        }
        if ($expectValid) {
            $this->assertTrue($timeLine->isValid());
        } else {
            $this->assertFalse($timeLine->isValid());
            $expectedArrayWithErrors[] = '';
        }

        // And check that the errors were generated correctly.
        $this->assertEquals($expectedArrayWithErrors, $timeLine->toArrayWithErrors());
    }

    public function compareWithPreviousLineProvider(): array
    {
        $thisLineContent = '10:00-11:00 = 1.00';

        return [
            'Previous after this one' => [$thisLineContent, '09:45-10:15 = 0.50', true, false],
            'Previous ends when this one starts' => [$thisLineContent, '09:15-10:00 = 0.75', false, false],
            'Gap between previous and this one' => [$thisLineContent, '08:15-09:00 = 0.75', false, false],
        ];
    }

    /**
     * @param string $thisContent
     * @param string $previousContent
     * @param bool $expectError
     * @param bool $expectGap
     * @dataProvider compareWithPreviousLineProvider
     */
    public function testCompareWithPreviousLine(
        string $thisContent,
        string $previousContent,
        bool $expectError,
        bool $expectGap
    ) {
        $thisLine = new TimeLine($thisContent);
        $previousLine = new TimeLine($previousContent);

        // Just check that they both start valid.
        $this->assertTrue($thisLine->isValid());
        $this->assertTrue($previousLine->isValid());

        // Call the method and check the outcome.
        $thisLine->compareWithPreviousLine($previousLine);
        if ($expectError) {
            $this->assertFalse($thisLine->isValid());
        } else {
            $this->assertTrue($thisLine->isValid());
            if ($expectGap) {
                $this->assertTrue($thisLine->isGapBefore());
            }
        }
    }
}
