<?php

namespace Tests\Lines;

use TimesheetParser\Lines\DescriptionLine;
use TimesheetParser\Lines\LineFactory;
use TimesheetParser\Lines\TimeLine;
use TimesheetParser\Lines\UnknownLine;
use DigiTicketsTests\AbstractTestCase;

class LineFactoryTest extends AbstractTestCase
{
    public function makeProvider(): array
    {
        return [
            'Valid time line 1' => ['09:00-10:00 = 1.00', TimeLine::class],
            'Valid time line 2' => ['15:15-16:45 = 1.50', TimeLine::class],
            'Valid time line, invalid length' => ['10:00-11:00 = 9.75', TimeLine::class],

            'Missing space 1' => ['10:00-11:00= 1.00', UnknownLine::class],
            'Missing space 2' => ['10:00-11:00 =1.00', UnknownLine::class],
            'Missing end time' => ['10:00-', UnknownLine::class],

            'Description line' => ['    Description line', DescriptionLine::class],

            'Invalid description line 1' => ['   Not enough leading spaces', UnknownLine::class],
            'Invalid description line 2' => ['     Too many leading spaces', UnknownLine::class],

            'Rubbish' => ['boom', UnknownLine::class],
        ];
    }

    /**
     * @param string $lineData
     * @param string $expectedClassName
     * @dataProvider makeProvider
     */
    public function testMake(string $lineData, string $expectedClassName)
    {
        $line = LineFactory::make($lineData);

        $this->assertEquals($expectedClassName, get_class($line), 'Incorrect Line class was instantiated');
    }
}
