<?php

namespace Tests\Timesheets;

use TimesheetParser\TimesheetValidator;
use DigiTicketsTests\AbstractTestCase;

class TimesheetValidatorTest extends AbstractTestCase
{
    public function testParseWithInvalidFilePath()
    {
        $this->expectException(\RuntimeException::class);
        TimesheetValidator::parse('does-not-exist');
    }

    public function parseProvider(): array
    {
        return [
            'simple, valid' => [
                'validNoBreaks.txt',
                true,
                [
                    '',
                    '09:00-10:15 = 1.25',
                    '    First task',
                    '10:15-12:00 = 1.75',
                    '    Second task',
                    '',
                    'Daily time: 3.00',
                    '--> 4.50 hours down across the period',
                    '',
                ],
            ],
            'valid with breaks and gaps' => [
                'validWithBreaksAndGaps.txt',
                true,
                [
                    '',
                    '11:00-12:15 = 1.25',
                    '    Third task',
                    '12:15-13:00 = 0.75',
                    '    Fourth task',
                    '',
                    '--- break here',
                    '',
                    '14:00-15:30 = 1.50',
                    '    Fifth Task',
                    '',
                    'Daily time: 3.50',
                    '--> 4.00 hours down across the period',
                    '',
                ],
            ],
            'invalid daily times' => [
                'invalidDailyTimes.txt',
                false,
                [
                    '',
                    '10:00-11:30 = 1.25',
                    '^-- Length is not correct. Should be 1.50',
                    '',
                    '    Task 1',
                    '12:15-13:00 = 0.50',
                    '^-- Length is not correct. Should be 0.75',
                    '',
                    '    Task 2',
                    '15:00-14:00 = 1.00',
                    '^-- Start time must not be later than end time',
                    '',
                    '    Task 3',
                ],
            ],
            'overlapping times' => [
                'overlappingTimes.txt',
                false,
                [
                    '',
                    '15:00-16:00 = 1.00',
                    '    Task 1',
                    '15:45-16:45 = 1.00',
                    '^-- This line starts before the previous line',
                    '',
                    '    Task 2',
                    '12:00-13:00 = 1.00',
                    '^-- This line starts before the previous line',
                    '',
                    '    Task 3',
                ],
            ],
            'Some not real lines' => [
                'someNotRealLines.txt',
                false,
                [
                    '',
                    '10:00-11:00 = 1.00',
                    'Haha, not a real line',
                    '^-- Line is of unknown type',
                    '',
                    '11:00 - not a real line, either',
                    '^-- Line is of unknown type',
                    '',
                    '12:00-13:00 = 1.00',
                    '15:00-16:00 = 1.00',
                    '    This is a real line',
                    '        This is not',
                    '^-- Line is of unknown type',
                    '',
                ],
            ],
            'One day with a date and length' => [
                'oneDayWithDateAndLength.txt',
                true,
                [
                    '',
                    '',
                    '01/06/2021 [8.00]',
                    '',
                    '09:00-12:30 = 3.50',
                    '    Morning',
                    '',
                    '--- break here',
                    '',
                    '13:30-18:00 = 4.50',
                    '    Afternoon',
                    '',
                    'Daily time: 8.00',
                    '--> 0.50 hours up across the period',
                    '',
                ],
            ],
            'One day with a date and invalid length' => [
                'oneDayWithDateAndInvalidLength.txt',
                false,
                [
                    '',
                    '',
                    '01/06/2021 [6.00]',
                    '^-- Daily time should be 8.00',
                    '',
                    '',
                    '09:00-12:30 = 3.50',
                    '    Morning',
                    '13:30-18:00 = 4.50',
                    '    Afternoon',
                ],
            ],
            'Multiple days' => [
                'multipleDays.txt',
                true,
                [
                    '',
                    '',
                    '01/06/2021 [7.00]',
                    '',
                    '09:00-12:30 = 3.50',
                    '    Morning',
                    '',
                    '--- break here',
                    '',
                    '13:30-17:00 = 3.50',
                    '    Afternoon',
                    '',
                    'Daily time: 7.00',
                    '',
                    '',
                    '02/06/2021 [7.75]',
                    '',
                    '08:45-12:30 = 3.75',
                    '    Morning',
                    '',
                    '--- break here',
                    '',
                    '13:30-17:30 = 4.00',
                    '    Afternoon',
                    '',
                    'Daily time: 7.75',
                    '',
                    'Total time: 14.75',
                    '--> 0.25 hours down across the period',
                    '',
                ],
            ],
            'Multiple days, wrong order' => [
                'multipleDaysWrongOrder.txt',
                false,
                [
                    '',
                    '',
                    '02/06/2021 [7.75]',
                    '',
                    '08:45-12:30 = 3.75',
                    '    Morning',
                    '13:30-17:30 = 4.00',
                    '    Afternoon',
                    '',
                    '',
                    '01/06/2021 [7.00]',
                    '^-- This date must be later than the previous day\'s date',
                    '',
                    '',
                    '09:00-12:30 = 3.50',
                    '    Morning',
                    '13:30-17:00 = 3.50',
                    '    Afternoon',
                ],
            ],
        ];
    }

    /**
     * @param string $filePath
     * @param bool $expectValid
     * @param array $expectedRelevantToArray
     * @dataProvider parseProvider
     */
    public function testParse(string $filePath, bool $expectValid, array $expectedRelevantToArray)
    {
        $timesheet = TimesheetValidator::parse(sprintf('%s/_fixtures/%s', __DIR__, $filePath));

        $this->assertEquals($expectValid, $timesheet->isValid());
        if ($timesheet->isValid()) {
            $this->assertEquals($expectedRelevantToArray, $timesheet->toArray());
        } else {
            $this->assertEquals($expectedRelevantToArray, $timesheet->errorsToArray());
        }
    }
}
