<?php

namespace TimesheetParser\Lines;

class UnknownLine extends AbstractLine
{
    public function __construct(string $content)
    {
        parent::__construct($content);

        $this->addError('Line is of unknown type');
    }
}
