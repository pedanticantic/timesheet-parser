<?php

namespace TimesheetParser\Lines;

class DateLine extends AbstractLine
{
    /**
     * @var \DateTimeImmutable
     */
    private $timesheetDate;

    /**
     * @var float|null
     */
    private $recordedLength;

    public function __construct(string $content)
    {
        parent::__construct($content);

        // Extract the date out of the line.
        $contentDatePart = substr($content, 0, 10);
        $dateString = $contentDatePart.' 00:00:00.000000';
        $this->timesheetDate = \DateTimeImmutable::createFromFormat('d/m/Y H:i:s.u', $dateString);
        if ($this->timesheetDate->format('d/m/Y') != $contentDatePart) {
            $this->addError('Date is invalid');
        }

        if (strlen($content) > 13) {
            $this->recordedLength = (float) substr($content, 12, 4);
        }
    }

    private function applyBlankLinesForOutput(array $lines): array
    {
        array_unshift($lines, '');
        array_push($lines, '');

        return $lines;
    }

    public function checkDailyTime(float $actualDailyTime)
    {
        if (!is_null($this->recordedLength)) {
            if ($this->recordedLength != $actualDailyTime) {
                $this->addError(sprintf('Daily time should be %.2f', $actualDailyTime));
            }
        }
    }

    public function getTimesheetDate()
    {
        return $this->timesheetDate;
    }

    public function toArray(): array
    {
        return $this->applyBlankLinesForOutput(parent::toArray());
    }

    public function toArrayWithErrors(): array
    {
        return $this->applyBlankLinesForOutput(parent::toArrayWithErrors());
    }
}
