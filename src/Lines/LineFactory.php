<?php

namespace TimesheetParser\Lines;

class LineFactory
{
    public static function make(string $rawLine): AbstractLine
    {
        if (preg_match('/^\d\d\/\d\d\/\d{4} \[(\d{1,2}.\d\d)?]$/', $rawLine) === 1) {
            return new DateLine($rawLine);
        } elseif (preg_match('/^\d\d:\d\d-\d\d:\d\d = \d\.\d\d$/', $rawLine) === 1) {
            return new TimeLine($rawLine);
        } elseif (preg_match('/^ {4}[^\s]/', $rawLine) === 1) {
            return new DescriptionLine($rawLine);
        }

        return new UnknownLine($rawLine);
    }
}
