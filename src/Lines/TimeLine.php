<?php

namespace TimesheetParser\Lines;

use DateTimeImmutable;

/**
 * This represents a line that holds the start, end and length of a stint.
 */
class TimeLine extends AbstractLine
{
    /**
     * @var DateTimeImmutable
     */
    private $startTime;

    /**
     * @var DateTimeImmutable
     */
    private $endTime;

    /**
     * @var bool Is there a time gap between the previous line ending and this one starting?
     */
    private $isGapBefore = false;

    public function __construct(string $content)
    {
        parent::__construct($content);

        // Parse and store the start/end times and the length.
        // We can be sure that the regexes below find the right things (and the right number of things).
        $matches = null;
        preg_match_all('/\d\d:\d\d/', $content, $matches);
        $startEndTimes = array_map(
            function (string $timeString) {
                return static::parseTime($timeString);
            },
            $matches[0]
        );
        $this->startTime = $startEndTimes[0];
        $this->endTime = $startEndTimes[1];
        // Handle a stint finishing at midnight.
        if ($this->endTime->format('H:i') === '00:00') {
            $this->endTime = $this->endTime->add(new \DateInterval('P1D'));
        }

        preg_match_all('/[\d]{1,2}\.\d\d/', $content, $matches);
        $this->length = array_map(
            function (string $timeString) {
                return (float) $timeString;
            },
            $matches[0]
        )[0];

        // Validate the start/end times and length in this line.
        $this->validateTimes();
    }

    private static function parseTime(string $timeString): DateTimeImmutable
    {
        // PHP doesn't have a "time" object, so we have to use a datetime and assume today's date.
        $today = new DateTimeImmutable();

        // Build a DateTimeImmutable object using the date from above and the time from the string.
        return new DateTimeImmutable(sprintf('%s %s:00.000000', $today->format('Y-m-d'), $timeString));
    }

    private function validateTimes()
    {
        // Check that the start time is not later than the end time.
        if ($this->startTime > $this->endTime) {
            $this->addError('Start time must not be later than end time');
        }
        // Check that the difference between the start & end times equals the given length.
        $diff = $this->startTime->diff($this->endTime);
        $hours = (int) $diff->format('%h');
        $minutes = (int) $diff->format('%i');
        $calculatedLength = (float) ($hours + ($minutes/60));
        if ($this->length !== $calculatedLength) {
            $this->addError(sprintf('Length is not correct. Should be %.2f', $calculatedLength));
        }
    }

    public function compareWithPreviousLine(TimeLine $previousLine)
    {
        $previousEndTime = $previousLine->getEndTime();
        if ($previousEndTime > $this->startTime) {
            $this->addError('This line starts before the previous line');
        } elseif ($previousEndTime < $this->startTime) {
            $this->isGapBefore = true;
        }
    }

    public function toArray(): array
    {
        $result = parent::toArray();

        if ($this->isGapBefore) {
            // There is a gap before this line. Output the indicator for it _before_ the content.
            $result = array_merge(
                [
                    '',
                    '--- break here',
                    '',
                ],
                $result
            );
        }

        return $result;
    }

    public function getStartTime(): DateTimeImmutable
    {
        return $this->startTime;
    }

    public function getEndTime(): DateTimeImmutable
    {
        return $this->endTime;
    }

    public function isGapBefore(): bool
    {
        return $this->isGapBefore;
    }
}
