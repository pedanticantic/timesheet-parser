<?php

namespace TimesheetParser\Lines;

class AbstractLine
{
    /**
     * @var string The original content of the line.
     */
    private $content;

    /**
     * @var array any errors associated with this line.
     */
    private $errors = [];

    /**
     * @var float
     */
    protected $length = 0.00;

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    protected function addError(string $errorMessage)
    {
        $this->errors[] = $errorMessage;
    }

    /**
     * This is a really bad way of doing this, but I can't think of a better way.
     */
    public function dateIsNotLaterThanPrevious()
    {
        $this->addError('This date must be later than the previous day\'s date');
    }

    public function isValid(): bool
    {
        return count($this->errors) === 0;
    }

    public function getLength(): float
    {
        return $this->length;
    }

    public function toArray(): array
    {
        return [$this->content];
    }

    public function toArrayWithErrors(): array
    {
        $result = [];

        $result[] = $this->content;
        $showBlankLine = false;
        foreach ($this->errors as $error) {
            $result[] = sprintf('^-- %s', $error);
            $showBlankLine = true;
        }
        if ($showBlankLine) {
            $result[] = '';
        }

        return $result;
    }
}
