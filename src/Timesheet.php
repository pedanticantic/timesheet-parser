<?php

namespace TimesheetParser;

use TimesheetParser\Lines\AbstractLine;
use TimesheetParser\Lines\DateLine;

class Timesheet
{
    // The default number of hours in a day; can be overridden by adding HOURS_IN_A_DAY to .env file.
    const DEFAULT_HOURS_IN_A_DAY = 7.50;

    /**
     * @var Day[]
     */
    private $days;

    /**
     * @var float
     */
    private $totalAcrossDays;

    /**
     * Timesheet constructor.
     * @param Day[] $days
     */
    private function __construct(array $days)
    {
        $this->days = $days;
        $this->totalAcrossDays = 0.00;
        foreach ($this->days as $day) {
            $this->totalAcrossDays += $day->getTotalTime();
        }

        // Validate that all the dates are strictly increasing.
        if ($this->isValid()) {
            // Do the cross-line validation.
            $this->validateDaysAcrossTime();
        }
    }

    /**
     * @param AbstractLine[] $lines
     */
    public static function buildFromLines(array $lines): Timesheet
    {
        // Start a new collection of lines. Loop through the lines. If/when we get to a date line, "complete" the
        // current set of date lines and start a new set.
        $days = [];
        $oneDayLines = [];
        foreach ($lines as $line) {
            if ($line instanceof DateLine) {
                if (count($oneDayLines)) {
                    // We have all the lines for a given date.
                    $days[] = new Day($oneDayLines);
                    $oneDayLines = [];
                }
            }
            $oneDayLines[] = $line;
        }
        if (count($oneDayLines)) {
            // We have some lines for the last date.
            $days[] = new Day($oneDayLines);
        }

        return new static($days);
    }

    private function validateDaysAcrossTime()
    {
        $previousDay = null;
        foreach ($this->days as $day) {
            if ($previousDay) {
                $day->checkDateAgainstPrevious($previousDay);
            }
            $previousDay = $day;
        }
    }

    public function isValid(): bool
    {
        // @TODO: Obviously this needs to be better (ie have a flag that gets set during parse).
        $errorDays = array_filter(
            $this->days,
            function (Day $oneDay) {
                return !$oneDay->isValid();
            }
        );

        return count($errorDays) === 0;
    }

    public function errorsToArray(): array
    {
        if ($this->isValid()) {
            throw new \RuntimeException('Timesheet has no errors!');
        }

        return array_merge(
            ... array_map(
                function (Day $oneDay) {
                    return $oneDay->errorsToArray();
                },
                $this->days
            )
        );
    }

    public function toArray(): array
    {
        if (!$this->isValid()) {
            throw new \RuntimeException('Cannot convert to array as there are errors');
        }

        $result = array_merge(
            ... array_map(
                function (Day $oneDay) {
                    return $oneDay->toArray();
                },
                $this->days
            )
        );

        // Only show the total time if there more than 1 day.
        if (count($this->days) > 1) {
            $result[] = '';
            $result[] = sprintf('Total time: %.2f', $this->totalAcrossDays);
        }
        // Say how much the user is up/down across the period.
        $hoursDiff = $this->totalAcrossDays - (count($this->days) * $this->getHoursInDay());
        $result[] = sprintf(
            '--> %.2f hour%s %s across the period',
            abs($hoursDiff),
            abs($hoursDiff) === 1.00 ? '' : 's',
            $hoursDiff >= 0 ? 'up' : 'down'
        );
        $result[] = '';

        return $result;
    }

    private function getHoursInDay(): float
    {
        return (float) $_ENV['HOURS_IN_A_DAY'] ?: self::DEFAULT_HOURS_IN_A_DAY;
    }
}
