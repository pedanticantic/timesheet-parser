<?php

namespace TimesheetParser;

use TimesheetParser\Lines\LineFactory;

class TimesheetValidator
{
    public static function parse(string $filePath): Timesheet
    {
        // Load the file (and check that it exists!).
        if (!file_exists($filePath)) {
            throw new \RuntimeException(sprintf('File does not exist (%s)', $filePath));
        }
        $timesheetContents = file_get_contents($filePath);

        return static::parseContents($timesheetContents);
    }

    private static function parseContents(string $timesheet): Timesheet
    {
        // Remove any blank lines.
        $lines = array_filter(
            explode("\n", $timesheet),
            function (string $line) {
                return !empty($line);
            }
        );

        // Convert each remaining line to an instance of a subclass of the abstract line. The subclass is determined
        // by the factory.
        $lines = array_map(
            function (string $rawLine) {
                return LineFactory::make($rawLine);
            },
            $lines
        );

        return Timesheet::buildFromLines($lines);
    }
}
