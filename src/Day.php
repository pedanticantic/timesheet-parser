<?php

namespace TimesheetParser;

use \TimesheetParser\Lines\AbstractLine;
use \TimesheetParser\Lines\DateLine;
use \TimesheetParser\Lines\TimeLine;

class Day
{
    /**
     * @var AbstractLine[]
     */
    private $lines;

    /**
     * @var float
     */
    private $totalTime;

    /**
     * @var \DateTimeImmutable
     */
    private $linesDate;

    /**
     * @param AbstractLine[] $lines
     */
    public function __construct(array $lines)
    {
        $this->lines = $lines;

        // Determine the date of this day.
        $this->determineDate();

        // Calculate the total time for the day.
        $this->totalTime = 0.00;
        foreach($this->lines as $line) {
            $this->totalTime += $line->getLength();
        }

        // Parse and validate the lines.
        if ($this->isValid()) {
            // Do the cross-line validation.
            $this->validateTimesAcrossDays();
            $this->validateTotalDailyTime();
        }
    }

    private function determineDate()
    {
        // Extract just the "Day" type lines out of the lines. If there are none, assume "today", otherwise get the
        // date from the first one.
        $dateLines = array_filter(
            $this->lines,
            function (AbstractLine $line) {
                return $line instanceof DateLine;
            }
        );
        if (count($dateLines) === 0) {
            $this->linesDate = \DateTimeImmutable::createFromFormat(
                'Y-m-d h:i:s.u',
                (new \DateTimeImmutable())->format('Y-m-d').' 00:00:00.000000'
            );
        } else {
            /** @var DateLine $representativeDateLine */
            $representativeDateLine = reset($dateLines);
            $this->linesDate = $representativeDateLine->getTimesheetDate();
        }
    }

    public function getTotalTime(): float
    {
        return $this->totalTime;
    }

    public function getLinesDate(): \DateTimeImmutable
    {
        return $this->linesDate;
    }

    public function isValid(): bool
    {
        // @TODO: Obviously this needs to be better (ie have a flag that gets set during parse).
        $errorLines = array_filter(
            $this->lines,
            function (AbstractLine $oneLine) {
                return !$oneLine->isValid();
            }
        );

        return count($errorLines) === 0;
    }

    public function errorsToArray(): array
    {
        // There are errors. Build an array of the lines plus relevant error messages and return it.
        $result = [];
        $result[] = '';
        foreach ($this->lines as $oneLine) {
            $result = array_merge(
                $result,
                $oneLine->toArrayWithErrors()
            );
        }

        return $result;
    }

    /**
     * This method takes the lines of type "time" and compares each line to the next. It makes sure no line ends
     * before the next one starts, and if there's a gap, it records the gap.
     */
    private function validateTimesAcrossDays()
    {
        // We only want the "times" type lines.
        $timesLines = array_filter(
            $this->lines,
            function (AbstractLine $oneLine) {
                return $oneLine instanceof TimeLine;
            }
        );

        // Loop through them, and (if not the first line) compare this line to the one before.
        $previousLineIndex = null;
        foreach ($timesLines as $lineIndex => $timesLine) {
            /** @var TimeLine $timesLine */
            if (!is_null($previousLineIndex)) {
                /** @var TimeLine $previousLine */
                $previousLine = $timesLines[$previousLineIndex];
                $timesLine->compareWithPreviousLine($previousLine);
            }
            $previousLineIndex = $lineIndex;
        }
    }

    /**
     * This method extracts just the "DateLine" type lines, and tells each one what the actual daily time is. Those lines
     * then compare that to the time in the line and add an error if it's not correct. In practice, there is only 1
     * "Date" line per day, and sometimes the Date line doesn't have the time.
     */
    private function validateTotalDailyTime()
    {
        // We only want the "times" type lines.
        /** @var DateLine $dateLines */
        $dateLines = array_filter(
            $this->lines,
            function (AbstractLine $oneLine) {
                return $oneLine instanceof DateLine;
            }
        );
        foreach ($dateLines as $dateLine) {
            $dateLine->checkDailyTime($this->totalTime);
        }
    }

    public function checkDateAgainstPrevious(Day $previousDay)
    {
        if ($previousDay->getLinesDate() >= $this->linesDate) {
            $firstLine = reset($this->lines);
            $firstLine->dateIsNotLaterThanPrevious();
        }
    }

    public function toArray(): array
    {
        if (!$this->isValid()) {
            throw new \RuntimeException('Cannot convert to array as there are errors');
        }

        $result = [];

        $result[] = '';
        foreach ($this->lines as $oneLine) {
            $result = array_merge(
                $result,
                $oneLine->toArray()
            );
        }
        $result[] = '';
        $result[] = sprintf('Daily time: %.2f', $this->totalTime);

        return $result;
    }
}
